NdsTiles is a plugin for QGIS (http://www.qgis.org)
that is showing the boundaries of NDS Tiles

![alt text](doc/NdsTiles.png)

Just check it out into your QGIS plugin folder and activate it within QGIS
C:\Users\\[your user name]\\.qgis2\python\plugins