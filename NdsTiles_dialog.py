# -*- coding: utf-8 -*-
"""
/***************************************************************************
 NdsTilesDialog
                                 A QGIS plugin
 Shows NDS tiles
                             -------------------
        begin                : 2016-09-02
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Stefan Scholz
        email                : stefan@roehrach.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import sys

from PyQt4 import QtGui, uic
from qgis.core import *

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'NdsTiles_dialog_base.ui'))



def UnpackMorton(mortonCode):
    x = 0
    y = 0
    
    for iTgt in range(0,30):
        x |= (mortonCode & 0x1) << iTgt
        mortonCode >>= 1
        y |= (mortonCode & 0x1) << iTgt
        mortonCode >>= 1
        
    x |= (mortonCode & 0x1) << 31
    y |= (y & (0x1 << 30)) << 1

    return [x, y]


def UnpackToTileNumber(packedTileId):
    layer = 0
    tilenum = sys.maxint
    layermask = 0x80000000
   
    for l in range(15, 1, -1) :
        layer = l
        if (layermask & packedTileId):
            break
        layermask >>= 1

    tileNum = packedTileId & (~layermask)
    return [ layer, tileNum ]
    
def TileNumberToTilePosition(tileNum, layer):
    [x,y] = UnpackMorton(tileNum)
    signshift = 31 - layer
    x <<= signshift
    x >>= signshift
    signshift += 1
    y <<= signshift
    y >>= signshift
    return [x,y]
    

def UnpackToTilePosition(packedTileId):
    [layer, tileNum] = UnpackToTileNumber(packedTileId)
    [gridX, gridY] = TileNumberToTilePosition( tileNum, layer)
    return [layer, gridX, gridY]
    
def GetWgsTileWidth(layer):
    return 180.0 / (0x1 << layer)
    
def GetWgsRectangle(layer, gX, gY):
    tileWidth = GetWgsTileWidth(layer)
    
    xl = float(gX * tileWidth)
    yl = float((gY+1) * tileWidth)
    xr = float((gX+1) * tileWidth)
    yr = float(gY * tileWidth)
    return [xl,yl,xr,yr]



class NdsTilesDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(NdsTilesDialog, self).__init__(parent)
        self.setupUi(self)
        self.btnShowTile.clicked.connect(self.showTile)

    def showTile(self):

        lines = [int(self.lineEdit.text())]
        layerName = 'Tile: ' + str(self.lineEdit.text())
        
        vl = QgsVectorLayer("Polygon?crs=EPSG:4326", layerName, "memory")
        pr = vl.dataProvider()

        features = []


        for line in lines:
            [layer, x, y] = UnpackToTilePosition(int(line))
            [xl,yl,xr,yr] = GetWgsRectangle(layer,x,y)
            polygon = QgsGeometry.fromPolygon([[QgsPoint(xl, yl), QgsPoint(xl, yr), QgsPoint(xr, yr), QgsPoint(xr,yl)]])
            fet = QgsFeature()
            fet.setGeometry(polygon)
            features.append(fet)

        pr.addFeatures(features)
        QgsMapLayerRegistry.instance().addMapLayer(vl) 
        
